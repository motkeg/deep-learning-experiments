provider "google" {
  credentials = "${file("./creds/main-dl.json")}"
  project     = "main-dl"
  region      = "us-central1"
}