resource "google_compute_instance" "gpu-vm" {
  count = 1
  name = "gpu-vm"
  machine_type = "n1-standard-4" // 1 CPU 16 Gig of RAM
  zone = "us-central1-a"  //"${var.region}" // Call it from variable "region"
  tags = ["http"]
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
      size = 50 // 50 GB Storage
    }
   }
  network_interface {
    network = "default"
      access_config {
        // Ephemeral IP - leaving this block empty will generate a new external IP and assign it to the machine
      }
  }
  guest_accelerator{
    type = "nvidia-tesla-k80" // Type of GPU attahced
    count = 1 // Num of GPU attached
  }
  scheduling{
    on_host_maintenance = "TERMINATE" // Need to terminate GPU on maintenance
  }
  metadata_startup_script = "${file("start-up.sh")}" // Here we will add the env setup
}