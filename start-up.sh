#!/bin/bash
echo "Checking for CUDA and installing."
# Check for CUDA and try to install.
if ! dpkg-query -W cuda-9-0; then
  # The 16.04 installer works with 16.10.
  curl -O http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
  dpkg -i ./cuda-repo-ubuntu1604_9.0.176-1_amd64.deb
  apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
  apt-get update
  apt-get install cuda-9-0 -y
fi
# Enable persistence mode
nvidia-smi -pm 1

# Install docker
apt-get update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install -y docker-ce

# Install nvidia-container-runtime
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
  apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  tee /etc/apt/sources.list.d/nvidia-docker.list
apt-get update

# Install nvidia-docker2 and reload the Docker daemon configuration
apt-get install -y nvidia-docker2
pkill -SIGHUP dockerd

# add ssh key
mkdir ~/.ssh

# echo "<ADD_YOUR_SSH_KEY_HERE>"  >> ~/.ssh/id_rsa_gitlab
# ssh-keyscan gitlab.com >>  ~/.ssh/known_hosts
# chmod 0400 ~/.ssh/id_rsa_gitlab
# eval "$(ssh-agent -s)"
#ssh-add ~/.ssh/id_rsa_gitlab

# Clone project repository
mkdir ~/datascience
cd ~/datascience
git clone git@gitlab.com:dice89/deep-learning-experiments.git

# Pull and run docker image
docker pull dice89/ubuntu-gpu-python-dl
docker run --runtime=nvidia  -d -p 80:8080 -v ~/datascience:/root/project  dice89/ubuntu-gpu-python-dl